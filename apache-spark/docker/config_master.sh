#!/bin/bash

echo "$MASTERIP spark-master" >> /etc/hosts
echo "export SPARK_WORKER_PORT=12346" >> /spark/conf/spark-env.sh
if [ ! -z "$SPARK_PUBLIC_DNS" ]; then
    echo "export SPARK_PUBLIC_DNS=$SPARK_PUBLIC_DNS" >> /spark/conf/spark-env.sh
fi
echo "spark.driver.port=12345" >> /spark/conf/spark-defaults.conf
export SPARK_WORKER_PORT=12346
export SPARK_PUBLIC_DNS=$SPARK_PUBLIC_DNS
/bin/bash /worker.sh
